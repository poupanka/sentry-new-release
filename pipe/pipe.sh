#!/usr/bin/env bash
#
# This pipe is an example to show how easy is to create pipes for Bitbucket Pipelines.
#

source "$(dirname "$0")/common.sh"

enable_debug
extra_args=""
if [[ "${DEBUG}" == "true" ]]; then
  extra_args="--verbose"
  export SENTRY_LOG_LEVEL=debug
fi

info "Executing the pipe..."

# Required parameters
SENTRY_AUTH_TOKEN=${SENTRY_AUTH_TOKEN:?'SENTRY_AUTH_TOKEN variable missing.'}
SENTRY_PROJECT=${SENTRY_PROJECT:?'SENTRY_PROJECT variable missing.'}
SENTRY_ORG=${SENTRY_ORG:?'SENTRY_ORG variable missing.'}

# Optional parameters
SHOULD_FINALIZE=${FINALIZE:-"true"}
ENVIRONMENT=${ENVIRONMENT}

export SENTRY_AUTH_TOKEN=$SENTRY_AUTH_TOKEN
export SENTRY_ORG=$SENTRY_ORG
export SENTRY_RELEASE=$BITBUCKET_TAG
export SENTRY_PIPELINE=bitbucket/0.3.0

sentry-cli releases new -p $SENTRY_PROJECT $SENTRY_RELEASE
sentry-cli releases set-commits --auto $SENTRY_RELEASE
if [[ -n "${ENVIRONMENT}" ]]; then
  sentry-cli releases deploys $SENTRY_RELEASE new -e $ENVIRONMENT
fi
if [[ "${SHOULD_FINALIZE}" == "true" ]]; then
  sentry-cli releases finalize $SENTRY_RELEASE
fi
